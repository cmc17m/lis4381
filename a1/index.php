<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Claire Coulter">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>	

		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>

		<?php include_once("global/header.php"); ?>	
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<style type="text/css">
							body
								{
  									padding-top: 50px;
								}
								
								h1
								{
									text-align: center;
									text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue;
  									color: white;
									margin: 0px;     
									padding: 0px;
									font-size: 48px;
									font-family: "Georgia", serif;    
								}
						</style>
				</div>
				<p class="text-justify">
					<h3>Requirements</h3> 
					<strong>Checklist (3 parts):</strong>

					<br>1. Distributed Version Control with Git and Bitbucket
					<br>2. Development Installations
					<br>3. Chapter Questions (Chapters 1, 2)
				</p>

				<br>	
				<h4>Java Installation</h4>
				<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/android.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!--starter-template -->
    	</div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
