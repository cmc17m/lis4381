> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Claire Coulter

### Assignment 1 Requirements:

3 parts:

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chapters 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links

#### Git commands w/short descriptions:

1. git init - creates a new Git repository
2. git status - displays working directory state as well as staging area
3. git add - add change in working directory to the staging area
4. git commit - saves your changes to the local repo
5. git push - uploads local repo content to a remote repo
6. git pull - updates the local version of a repo from a reomte
7. git clone - targets an existing repo and creates a clone 
 

#### Assignment Screenshots:

*Screenshot of AMPPS running* *http://localhost/cgi-bin/phpinfo.cgi*

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cmc17m/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")


