<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Claire Coulter">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>

		<?php include_once("global/header.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<style type="text/css">
							body
								{
  									padding-top: 50px;
								}
								
								h1
								{
									text-align: center;
									text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue;
  									color: white;
									margin: 0px;     
									padding: 0px;
									font-size: 48px;
									font-family: "Georgia", serif;    
								}
						</style>
				</div>
				
				<p class="text-justify">
					<h3>Basic server-side validation</h3>
					<h5>PHP server-side validation, along with prepared statements to help prevent SQL injection</h5>
					<h5>Adds edit/delete functionality to A5.</h5>
					<p>(Table: petstore)</p>
					<h2 class="top">Error!</h2>
					<p>Name can only contain letters, numbers, hyphens, and underscore.</p>
				</p>


				<br>
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
