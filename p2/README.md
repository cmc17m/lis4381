> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Claire Coulter

### Project 2 Requirements:

*Checklist:*

1. Create PHP server-side validation of petstore tabel values
2. Display user entered data and permits user to delete data
3. Answer Chapter questions

#### README.md file should include the following items:

* Course-title, name, and Project Requirements
* Screenshot of home page carousel
* Screenshots of PHP server-side validation 
* Screenshots of failed validation error message

#### Project Screenshots:

*Screenshot of Home Page Carousel*:

![Home Page](img/carousel.png)

*Screenshot of edit_petstore.php*:

![JDK Installation Screenshot](img/editpetstore.png)

*Screenshot of Failed Validation*:

![Android Studio Installation Screenshot](img/failedval.png)

*My Online Portfolio Link:*
[Portfolio](http://localhost/repos/lis4381/ "My Online Portfolio")