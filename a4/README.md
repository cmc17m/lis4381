> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381
## Claire Coulter

### Assignment 4 Requirements:

*Checklist:*

1. Design online portfolio
2. Display screenshots of running Pet Store validation
3. Display screenshot of portfolio home page
4. Answer Chapter questions
5. Display screenshots of Skill Sets 

#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshots of running validation checks
* Screenshots of Skill Sets

#### Assignment Screenshots:

*Online Portfolio Home Page*:                      

![Home Page Screenshot](img/homepage2.png)

|    *Screenshot of Failed User Verification:*         |    *Screenshot of Successful User Validation:*      |
|------------------------------------------------|----------------------------------------------|
|![Failed User Ver](img/failed.png)   |  ![Successful User Ver](img/verified.png)|

|    *Screenshot of Skill Set 10:*         |    *Screenshot of Skill Set 11:*   
|------------------------------------------------|----------------------------------------------|
|![Skill Set 10](img/ss10.png)   |  ![Skill Set 11](img/ss11.png)|  

*Screenshot of Skill Set 12:*
![Skill Set 12](img/ss12.png)

*My Online Portfolio Link:*
[Portfolio](http://localhost/repos/lis4381/ "My Online Portfolio")
