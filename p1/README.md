> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Claire Coulter

### Project 1 Requirements:

*Checklist:*

1. Create a mobile app business card with Android Studio
2. Display screenshots of first and second user interfaces
3. Answer questions for Chapters 7 & 8
4. Display screenshots of Skill Sets 7-9

#### README.md file should include the following items:

* Course title, name, and Assignment Requirements
* Screenshots of running application for user interfaces
* Screenshots of Skill Sets 7-9

#### Assignment Screenshots:

|   Screenshot of first user interface:   |   Screenshot of second user interface:   |
|-----------------------------------------|------------------------------------------|
|![First User Interface](img/firstuserint.png)   | ![Second User Interface](img/seconduserint.png)  |  

|   Screenshot of Skill Set 7:   |   Screenshot of Skill Set 8:   |   Screenshot of Skill Set 9: | 
|----------------------------------------------|-------------------------------------------------|-------------------------------------------------|
|![Even or Odd](img/ss7.png)               | ![Largest Number](img/ss8.png)              | ![Arrays and Loops](img/ss9.png)


