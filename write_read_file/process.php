<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
?>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Claire Coulter">
	<link rel="icon" href="favicon.ico">

    <title>Write/Read File</title>
    <?php include_once("../css/include_css.php"); ?>
    </head>

    <body>

    <?php include_once("../global/nav.php"); ?>

    <?php include_once("global/header.php"); ?>

    <div class="container">
      <div class="starter-template">
        <div class="page-header">
          <style type="text/css">
                        body
                            {
                                padding-top: 50px;
                            }
                            
                            h1
                            {                                    
                                text-align: center;
                                text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue;
                                color: white;
                                margin: 0px;     
                                padding: 0px;
                                font-size: 48px;
                                font-family: "Georgia", serif;    
                            }       
                </style>
        </div>
<p class="text-justify">
   <?php
   $myfile = fopen("file.txt", "w+") or exit("Unable to open file!");
   $txt = $_POST['comment'];
   fwrite($myfile, $txt);
   fclose($myfile);
   $myfile = fopen("file.txt", "r+") or exit("Unable to open file!");

   while(!feof($myfile)){
       echo fgets($myfile) . "<br/>";
   }
   fclose($myfile);
   ?>
    </p>
    <?php include_once "global/footer.php"; ?>

    </div>
    </div>

    <?php include_once("../js/include_js.php"); ?>
    <script>
    $(document).ready(function(){
        $('#myTable').DataTable({
            responsive:true
        });
    });
    </script>
    </body>
    </html>


    
    
