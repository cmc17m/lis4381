<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Claire Coulter">
	<link rel="icon" href="favicon.ico">

    <title>Write/Read File</title>
    <?php include_once("../css/include_css.php"); ?>
    </head>

    <body>

    <?php include_once("../global/nav.php"); ?>

    <?php include_once("global/header.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <style type="text/css">
                        body
                            {
                                padding-top: 50px;
                            }
                            
                            h1
                            {                                    
                                text-align: center;
                                text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue;
                                color: white;
                                margin: 0px;     
                                padding: 0px;
                                font-size: 48px;
                                font-family: "Georgia", serif;    
                            }       
                </style>

            </div>

<h2>File Data</h2>
<form class="form-horizontal" role="form" method="post" action="process.php">
<div class="form-group">
<label class="control-label col-sm-2" for="comment">Comment:</label>
<div class="col-sm-10">
<input class="form-control" rows= "5" name="comment" id="comment" placeholder="Please enter text here...">
</div>
</div>


<div class="form-group">
<div class="col-sm-12">
<button type="submit" class="btn btn-default">Submit</button>
</div>
</div>
</form>

<?php include_once "global/footer.php"; ?>

    </div>
    </div>

<?php include_once("../js/include_js.php"); ?>

<script>
$(document).ready(function(){
    $('#myTable').DataTable({
        responsive: true
    });
    });
</script>
</body>
</html>

