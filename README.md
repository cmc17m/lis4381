> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Claire Coulter

### LIS4381 Requirements:

*Course Work Links:*

#### 1. [A1 README.md](a1/README.md "My A1 README.md file")
  -  Install AMPPS
  -  Install JDK
  -  Install Android Studio and create My First App
  -  Provide screenshots of installations
  -  Create Bitbucket repo
  -  Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
  -  Provide git command descriptions
#### 2. [A2 README.md](a2/README.md "My A2 README.md file")
  - Create Healthy Recipes Android app
  - Provide screenshots of completed app
#### 3. [A3 README.md](a3/README.md "My A3 README.md file")
  - Create ERD based upon business rules
  - Provide screenshot of completed ERD
  - Provide DB resource links
#### 4. [A4 README.md](a4/README.md "My A4 README.md file")
  - Create My Online Portfolio
  - Provide Screenshots of home page and successful/failed verification
  - Provide link to online portfolio

#### 5. [A5 README.md](a5/README.md "My A5 README.md file")
  - Create Basic Server-side Validation 
  - Provide Screenshots of validation and error message
  - Provide link to online portfolio

#### 6. [P1 README.md](p1/README.md "My P1 README.md file")
- Create My Business Card Android App
- Provide screenshots of completed app

#### 7. [P2 README.md](p2/README.md "My P2 README.md file")
- Create PHP server-side validation
- Provide screenshots of successful validation, failed validation error message
- Provide screenshots of RSS Feed and successfully deleted record
    

