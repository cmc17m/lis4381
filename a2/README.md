> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Claire Coulter

### Assignment 2 Requirements:

*Checklist:*

1. Create a mobile app recipe with Android Studio
2. Display screenshots of first and second user interfaces
3. Answer questions for Chapters 3 & 4
4. Display screenshots of Skill Sets 1-3

#### README.md file should include the following items:

* Course title, name, and Assignment Requirements
* Screenshots of running application for user interfaces
* Screenshots of Skill Sets 1-3

#### Assignment Screenshots:

|   Screenshot of first user interface:   |   Screenshot of second user interface:   |
|-----------------------------------------|------------------------------------------|
|![First User Interface](img/firstuserint.png)   | ![Second User Interface](img/seconduserint.png)  |  

|   Screenshot of Skill Set 1 (Even or Odd):   |   Screenshot of Skill Set 2 (Largest Number):   |   Screenshot of Skill Set 3 (Arrays and Loops): | 
|----------------------------------------------|-------------------------------------------------|-------------------------------------------------|
|![Even or Odd](img/evenodd.png)               | ![Largest Number](img/largest.png)              | ![Arrays and Loops](img/array.png)             |                                  |



