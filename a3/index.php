<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Claire Coulter">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>

		<?php include_once("global/header.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<style type="text/css">
							body
								{
  									padding-top: 50px;
								}
								
								h1
								{
									text-align: center;
									text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em blue;
  									color: white;
									margin: 0px;     
									padding: 0px;
									font-size: 48px;
									font-family: "Georgia", serif;    
								}
						</style>
				</div>
				<p class="text-justify">
					<h3>Requirements</h3> 
					<strong>Checklist (5 parts):</strong>

					<br>1. Design database solution in MySQL
					<br>2. Create a mobile app ticket price calculator in Android Studio
					<br>3. Display screenshots of ERD as well as first and second user interfaces
					<br>4. Answer Chapter 5 and 6 questions
					<br>5. Display screenshots of Skill Sets  
				</p>

				<br>	
				<h4>First User Interface:</h4>
				<img src="img/uncalculated.png" class="img-responsive center-block" alt="First User Int">

				<h4>Second User Interface:</h4>
				<img src="img/calculated.png" class="img-responsive center-block" alt="Second User Int">

				<h4>ERD:</h4>
				<img src="img/a3.png" class="img-responsive center-block" alt="ERD">

				<h4>Skill Set 5 (Part 1):</h4>
				<img src="img/ss5.1.png" class="img-responsive center-block" alt="Skill Set 5">

				<h4>Skill Set 5 (Part 2):</h4>
				<img src="img/ss5.2.png" class="img-responsive center-block" alt="Skill Set 5">

				<h4>Skill Set 5 (Part 3):</h4>
				<img src="img/ss5.3.png" class="img-responsive center-block" alt="Skill Set 5">

				<h4>Skill Set 6:</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="Skill Set 6">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
