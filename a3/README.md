> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381
## Claire Coulter

### Assignment 3 Requirements:

*Checklist:*

1. Design database solution in MySQL
2. Create a mobile app ticket price calculator in Android Studio
3. Display screenshots of ERD as well as first and second user interfaces
4. Answer Chapter 5 and 6 questions
5. Display screenshots of Skill Sets 

#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshots of running application for user interfaces
* Screenshots of Skill Sets
* Screenshot of ERD

#### Assignment Screenshots:

|    Screenshot of first user interface:         |    Screenshot of second user interface:      |
|------------------------------------------------|----------------------------------------------|
|![First User Interface](img/uncalculated.png)   |  ![Second User Interface](img/calculated.png)|

*Screenshot of ERD*:                      

![ERD Screenshot](img/a3.png)

|     Screenshot of First Skill Set:        |  Screenshot of First Skill Set Pt. 2:  |  Screenshot of First Skill Set Pt. 3:  |
|-------------------------------------------|----------------------------------------|----------------------------------------|
|![First Screenshot](img/ss5.1.png)         |   ![Second Screenshot](img/ss5.2.png)  |  ![Third Screenshot](img/ss5.3.png)    |

*Screenshot of Second Skill Set*:
![Skill Set 6 Screenshot](img/ss6.png)                             


#### MySQL Links:

*SQL Link:*
[A3 SQL](docs/a3.sql "A3 SQL")

*Workbench Link:*
[A3 MWB](docs/a3.mwb "A3 Workbench")


