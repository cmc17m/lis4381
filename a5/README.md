> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 

## Claire Coulter

### Assignment 5 Requirements:

*Checklist:*

1. Create server-side validation table of petstore values and prepared statements
2. Displays user entered data and permits user to enter data
3. Answer Chapter questions
4. Display screenshots of Skill Sets

#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshots of server-side validation running
* Screenshot of error message 
* Screenshots of Skill Sets

#### Assignment Screenshots:

*Screenshot of working Server-side validation*:

![Server-side Validation](img/serverside.png)

*Screenshot of Error message*:

![Error Message Screenshot](img/error.png)

*Screenshot of Skill Set 13*:

![Skill Set 13](img/ss13.png)

|    *Screenshot of Skill Set 14 (Pt. 1) :*         |    *Screenshot of Skill Set 14 (Pt. 2) :*   
|------------------------------------------------|----------------------------------------------|
|![Skill Set 14](img/firstss14.png)   |  ![Skill Set 14](img/secondss14.png)|  

|    *Screenshot of Skill Set 15 (Pt. 1) :*         |    *Screenshot of Skill Set 15 (Pt. 2) :*   
|------------------------------------------------|----------------------------------------------|
|![Skill Set 14](img/firstss15.png)   |  ![Skill Set 14](img/secondss15.png)|  

*My Online Portfolio Link:*
[Portfolio](http://localhost/repos/lis4381/ "My Online Portfolio")



